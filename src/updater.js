class WatchFile {
    constructor(filename) {
        this.filename = filename;
        this.content = null;
        this.changed = false;
        this.update();
        this.reset();
    }
    async update() {
        this.reset();
        let previous = this.content;
        this.content = await (await fetch(this.filename)).text();
        if (previous !== this.content) {
            this.changed = true;
        }
    }
    reset() {
        this.changed = false;
    }
    somethingChanged() {
        return this.changed;
    }
}
window.addEventListener("load", () => {
    let files = ["lernmaterialien.js", "style.css", "index.html"].map(filename => new WatchFile(filename));
    let duration = 1000;
    setInterval(() => {
        files.forEach(async (file) => {
            await file.update();
            if (file.somethingChanged()) {
                window.location.href = window.location.href;
                console.log(`changed something in file ${file.filename}`);
                file.reset();
            }
        });
    }, duration);
});
//# sourceMappingURL=updater.js.map