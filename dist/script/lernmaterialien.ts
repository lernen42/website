
window.addEventListener("load", () => {
    
    let links = document.getElementsByTagName( "a" );
    Array.prototype.forEach.call(links, link => {
        link.addEventListener( "click", e => {
            e.preventDefault();
            flyToPage( e.target.href );
        })
    })

});



async function flyToPage( url: string ): Promise<void> {
    console.log( "%cyou clicked me O_O", "color: #d63031; font-weight: bold; font-size: 2em" );
    await delay( 2000 );
    window.location.href = url;
}


function delay( duration: number ): Promise<void> {
    return new Promise( resolve => {
        setTimeout( () => resolve(), duration);
    });
}