
class WatchFile {
    private content: string = null;
    private changed = false;
    constructor( public filename: string ) {
        this.update();
        this.reset();
    }
    async update(): Promise<void> {
        this.reset();
        let previous = this.content;
        this.content = await (await fetch( this.filename )).text();
        if ( previous !== this.content ) {
            this.changed = true;
        }
    }
    reset(): void {
        this.changed = false;
    }
    somethingChanged(): boolean {
        return this.changed;
    }
}

window.addEventListener("load", () => {
    let files = [ "lernmaterialien.js", "style.css", "index.html" ].map( filename => new WatchFile( filename ));
    let duration = 1000;
    setInterval(() => {
        files.forEach( async file => {
            await file.update();
            if ( file.somethingChanged() ) {
                window.location.href = window.location.href;
                console.log( `changed something in file ${file.filename}`)
                file.reset();
            }
        });
    }, duration);
});