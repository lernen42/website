// Variable, die auf das Preisausschreiben Formular zeigt
let form = document.forms["preisausschreiben"];
// Change-Event Listener auf die Kundenkennung setzen
// (bei jeder Änderung an der Kundenkennung wird check() aufgerufen)
form.kundenkennung.addEventListener("change", check);
// Direkt einmal check() aufrufen
check();

// Diese check() Methode prüft Anforderung 8. aus der Anforderung:
// Die beiden ersten Großbuchstaben bei der Kundenkennung müssen mit den ersten
// Buchstaben von Vorname und von Nachname übereinstimmen
function check() {
  if (form.kundenkennung.value.length > 0 &&
    form.kundenkennung.value.slice(0, 2) != (form.vorname.value.charAt(0) + form.nachname.value.charAt(0)).toUpperCase()) {
    form.kundenkennung.setCustomValidity("Die Kundenkennung ist falsch");
    form.passwort.setAttribute("required", "");
  } else {
    form.kundenkennung.setCustomValidity("");
    form.passwort.removeAttribute("required");
  }
}

// Direkt bevor das Formular übertragen werden soll, wird hier noch einmal geprüft,
// ob alles okay ist und gegebenenfalls wird ein Veto eingelegt
form.addEventListener("submit", function(e) {
  // Prüfe Kundenkennung
  if (form.kundenkennung.value.length > 0 &&
    form.kundenkennung.value.slice(0, 2) != (form.vorname.value.charAt(0) + form.nachname.value.charAt(0)).toUpperCase()) {
    alert("Die Kundenkennung ist falsch");
    e.preventDefault(); // Übertragung abbrechen
    return;
  }
});
